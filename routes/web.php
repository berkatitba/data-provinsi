<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\DistrictController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route untuk Provinsi
Route::get('/provinces', [App\Http\Controllers\ProvinceController::class, 'index'])->name('index');

Route::get('/provinces/add', [App\Http\Controllers\ProvinceController::class, 'add'])->name('provinces.add');
Route::post('/provinces/store', [App\Http\Controllers\ProvinceController::class, 'store'])->name('provinces.store');

Route::get('/provinces/edit/{id}', [App\Http\Controllers\ProvinceController::class, 'edit'])->name('provinces.edit');
Route::post('/provinces/update/{id}', [App\Http\Controllers\ProvinceController::class, 'update'])->name('provinces.update');

Route::get('/provinces/delete/{id}', [App\Http\Controllers\ProvinceController::class, 'delete'])->name('provinces.delete');

//Route untuk Kebupaten
Route::get('/districts', [App\Http\Controllers\DistrictController::class, 'index'])->name('index');

Route::get('/districts/add', [App\Http\Controllers\DistrictController::class, 'add'])->name('districts.add');
Route::post('/districts/store', [App\Http\Controllers\DistrictController::class, 'store'])->name('districts.store');

Route::get('/districts/edit/{id}', [App\Http\Controllers\DistrictController::class, 'edit'])->name('districts.edit');
Route::post('/districts/update/{id}', [App\Http\Controllers\DistrictController::class, 'update'])->name('districts.update');

Route::get('/districts/delete/{id}', [App\Http\Controllers\DistrictController::class, 'delete'])->name('districts.delete');