<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kabupaten</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

        <body class="bg-gradient-primary">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-12 col-md-9">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">                        
                            <div class="col-lg-100">
                                <div class="p-4">
                                                
                                    <form action="{{route('districts.store')}}" method="post" enctype ="multipart/form-data" >
                                        <h2 class = 'text-center'>Tambah Kabupaten</h2>
                                        @csrf
                                    
                                        <div class="form-group">
                                            <label >Kode</label>
                                            <input type="text" name="code_dist" class ="form-control" placeholder="Kode Kabupaten" required>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label >Nama Kabupaten</label>
                                            <input type="text" name="name_dist" class="form-control" placeholder="Nama Kabupaten" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Provinsi</label>
                                            <select name="prov_id" class="form-control">
                                                <option disabled selected>
                                                    - Pilih Provinsi-
                                                </option>
                                                @foreach($prov_name as $row)
                                                    <option value="{{$row->id}}">{{$row->name_prov}}</option>
                                                @endforeach
                                            </select>
                                        </div>    

                                        <input type="submit" value="Simpan" class="btn btn-outline-primary">

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</html>