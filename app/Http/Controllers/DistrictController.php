<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\District;
use App\Models\Province;


class DistrictController extends Controller
{
    public function index()
    {    
    
        $district = District::select("districts.*",'provinces.name_prov as prov_name')
            -> join('provinces', 'provinces.id', '=', 'districts.prov_id')
            ->orderBy('districts.id')
            ->paginate(10);
        return view('districts.list', compact('district'));
    }

    public function add()
    {
        $prov_name = Province::all();
        return view('districts.add', compact('prov_name'));
    }      

    public function store(Request $request)
    {
        $district = new District();
        $district->code_dist =$request->code_dist;
        $district->name_dist =$request->name_dist;
        $district->prov_id = $request->prov_id;

        try {
            $district->save();
            return redirect('districts');
        }
        catch(\Exception $e)
        {
            return redirect('districts');
        }
    }

    public function edit($id){
        $prov_name = Province::all();
        $district= District :: findOrFail($id);
        return view('districts.edit', compact('prov_name','district'));
    }

    public function update(Request $request ,$id){

        $district= District :: findOrFail($id);
        $district->code_dist =$request->code_dist;
        $district->name_dist =$request->name_dist;
        $district->prov_id = $request->prov_id;

        try {
            $district->save();
            return redirect('districts');
        }
        catch(\Exception $e)
        {
            return redirect('districts');
        }
    }

    public function delete($id){
        $district = District ::findOrFail($id);
        try {
            $district->delete();
            return redirect('districts');
        }
        catch(\Exception $e)
        {
            return redirect('districts');
        }
    }
}
