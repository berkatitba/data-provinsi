<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;

class ProvinceController extends Controller
{
    public function index()
    {
        $province = Province::paginate(10);
        return view('provinces.list', compact('province'));
    }

    public function add()
    {
        return view('provinces.add');
    }

    public function store(Request $request){

        $province = new Province();
        $province->code_prov = $request->code_prov;
        $province->name_prov = $request->name_prov;
        try {
            $province->save();
            return redirect('provinces')->with('flash_message', 'Data Berhasil Ditambahkan!'); 
        }
        catch(\Exception $e)
        {
            return redirect('provinces')->with('flash_message', 'Data Berhasil Ditambahkan!'); 
        }
    }

    public function edit($id){
        $province = Province::FindOrFail($id);
        return view('provinces.edit', compact('province'));
    }

    public function update(Request $request ,$id){
        $province= Province ::findOrFail($id);
        $province->code_prov =$request->code_prov;
        $province->name_prov =$request->name_prov;

        try {
            $province->save($request->all());
            return redirect('provinces'); 
        }
        catch(\Exception $e)
        {
            return redirect('provinces'); 
        }
    }

    public function delete($id){
        $province = Province ::findOrFail($id);
        try {
            $province->delete();
            return redirect('provinces');
        }
        catch(\Exception $e)
        {
            return redirect('provinces');
        }
    }
}
