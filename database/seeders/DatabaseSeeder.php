<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //province
        DB::table('provinces')->insert([
            'code_prov' => 'PR001',
            'name_prov' => 'Nanggroe Aceh Darussalam',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR002',
            'name_prov' => 'Sumatra Utara',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR003',
            'name_prov' => 'Sumatera Selatan',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR004',
            'name_prov' => 'Sumatra Barat',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR005',
            'name_prov' => 'Bengkulu',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR006',
            'name_prov' => 'Kepulauan Riau',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR007',
            'name_prov' => 'Jambi',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR008',
            'name_prov' => 'Lampung ',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR009',
            'name_prov' => 'Bangka Belitung',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR010',
            'name_prov' => 'Banten',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR011',
            'name_prov' => 'DKI Jakarta',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR012',
            'name_prov' => 'Jawa Barat',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR013',
            'name_prov' => 'Jawa Tengah ',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR014',
            'name_prov' => 'Daerah Istimewa Yogyakarta',
        ]);
        DB::table('provinces')->insert([
            'code_prov' => 'PR015',
            'name_prov' => 'Jawa Timur',
        ]);


        //districs
        DB::table('districts')->insert([
            'code_dist' => 'KAB01',
            'name_dist' => 'Nias Utara',
            'prov_id' => '2',
        ]);
        DB::table('districts')->insert([
            'code_dist' => 'KAB02',
            'name_dist' => 'Sleman',
            'prov_id' => '14',
        ]);

    }
}
